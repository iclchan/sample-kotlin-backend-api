package com.aswissbank.endpoints

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.util.*

@RestController
class ExampleEndpoints {

  @GetMapping("/public")
  fun publicEndpoint(): String {
    return "You have reached the PUBLIC endpoint"
  }

  @GetMapping("/secured")
  fun securedEndpoint(
    @RequestHeader(name = "Authorization") authorizationHeader: String?,
  ): SecuredEndpointResponse {
    val token = authorizationHeader?.split(" ")?.get(1)
    val tokenParts = token?.split(".")
    tokenParts?.let {
      val objectMapper = ObjectMapper()
      val header: JsonNode = objectMapper.readTree(decode(tokenParts[0])?.toByteArray())
      val payload: JsonNode = objectMapper.readTree(decode(tokenParts[1])?.toByteArray())
      return SecuredEndpointResponse(
        accessToken = token,
        decodedAccessToken = DecodedAccessToken(
          header = header,
          payload = payload
        )
      )
    }
    throw ResponseStatusException(401, "Unauthorized: Unable to parse access token", null)
  }

  fun decode(encodedString: String?): String? {
    return String(Base64.getUrlDecoder().decode(encodedString))
  }
}

data class SecuredEndpointResponse(
  val accessToken: String?,
  val decodedAccessToken: DecodedAccessToken?,
)

data class DecodedAccessToken(
  val header: JsonNode,
  val payload: JsonNode,
)