package com.aswissbank.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SwaggerConfig {
  @Bean
  fun springShopOpenAPI(): OpenAPI {
    val info = Info().title("Sample Kotlin Springboot Kotlin Application")
      .description("Prototype API")
      .version("v0.0.1")
    return OpenAPI()
      .info(info)
  }
}

